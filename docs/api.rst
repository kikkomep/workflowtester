.. _api:

:mod:`wft4galaxy` --- WorkflowTester for Galaxy API
=============================================

.. automodule:: wft4galaxy
    :members:
